<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'clip_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://clip.test');
define('WP_SITEURL','http://clip.test');

//
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N_<|g)VBbT,V$j8Rg3-n$Bm+Z0,heKW8DL@a@@fK}#1_&09Y NO|;l/)qj[l#okr');
define('SECURE_AUTH_KEY',  'bSc_H7-n0-]wLrH+V/l:K`S@itF#mpPV|DF-V(O[an/?{WfSJaFahv{v4pfE.a<e');
define('LOGGED_IN_KEY',    'I?l]dMaXJD]LH+MZJ_j7pDuKDRegepEtXvQre$1g*9o!YO=@I -0D/hwsz6_L/hX');
define('NONCE_KEY',        '@kyw4?j#fg2wNobI.l}L#3! rIc!s_,xwn;WwWf a[rS4cP}^F{#.B^#[7fIdcGp');
define('AUTH_SALT',        'iqD*:WVSV0{?)FAE(=r?I[}lo;(?Hq:@bZ]lO!;-}`,kYdg3L,bLtUvN#O XVj7B');
define('SECURE_AUTH_SALT', 'yMZ28D>D>j:m$|{t?m;dk%YfZvoHT_A(yy2{mrs4P#E*q4hu8])D}1D5.N$g+%yT');
define('LOGGED_IN_SALT',   '=5>@Z-7H|Q2^}<DDx_hEyBBS?ua15m~XwP[?s7#Y>4`IUn,_X_[<~qtNGu]QxNe#');
define('NONCE_SALT',       'E* f~%Fn9WW6g0VgfE_!LYr4S}#rRp_cL.bOL*do}TX!35et9_vv|o!q?-^g9 Q+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
