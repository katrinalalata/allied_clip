<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<!--HOME - SPLASH-->

<section class="home_splash inner_splash" style="background-image:url(<?php the_field('hero_image'); ?>);">
    <div class="site_container">
        <div class="splash_content">
            <h1><?php the_title()?></h1>
        </div>
    </div>
        
</section>

<section class="slides">
    <div class="site_container">
            <div class="inner_content">
                <?php the_field('content'); ?>
                
            </div>
        </div>
</section>



                    <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12024.233657967718!2d-74.1339685!3d41.1114153!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf136465d7d79dda!2sDr%20Janice%20Montague%2C%20Tuxedo%20Pediatrics!5e0!3m2!1sen!2sus!4v1579211996762!5m2!1sen!2sus" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>


<script>

    
$(function() {
    var header = $("#sticker");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 200) {
            header.addClass("darkHeader");
        } else {
            header.removeClass("darkHeader");
        }
    });
    
//    $('input').bind('focus blur', function() {
//    $('.contact_home').addClass('blend');
//    });
//     $('textarea').bind('focus blur', function() {
//        $('.contact_home').addClass('blend');
//    });
    
});
</script>
<?php
get_footer();
