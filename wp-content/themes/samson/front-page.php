<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<!--HOME - SPLASH-->

<section class="home_splash" style="background-image: url(<?php the_field('banner'); ?>);">
    <div class="site_container">
        <div class="splash_content">
            <h1><?php the_field('banner_title'); ?></h1>
            <h3><?php the_field('banner_subtitle'); ?></h3>
        </div>
    </div>
        
</section>

<section class="samson_stages">
    <div class="site_container">
        <div class="words_candy_standard">
            <div class="words words_long">
                <h2><span class="border_red"><?php the_field('intro_title'); ?></span></h2>
                <?php the_field('introduction'); ?>
            </div>
        </div>
        <div class="intro_clips">
            <div class="clip">
                <h3>Experienced Doctors</h3>
                <p>All of our Doctors are Board Certified in Pediatrics and have more than 75 years of collective experience. You can rest assured your children are in good hands. </p>
            </div>
                        <div class="clip">
                <h3>Professional & Friendly Staff</h3>
                <p>Our staff will ensure a great experience from start to finish. No matter what questions or concerns you may have, you will be treated with kindness and respect. </p>
            </div>
                        <div class="clip">
                <h3>Evening Hours</h3>
                <p>As a working parent, we know it can be challenging to get to the office during regular hours, so we are open until 7pm from Monday to Thursday for your convenience.</p>
            </div>
                        <div class="clip">
                <h3>Same Day Appointments</h3>
                <p>When your child wakes up feeling ill, give us a call and we will get them into the schedule that day. This way they can get back to their old selves ASAP.</p>
            </div>
                        <div class="clip">
                <h3>On-Call 24/7</h3>
                <p>After office hours our on-call service is ready to answer your questions and concerns around the clock. Someone is always available when you need support. </p>
            </div>
                        <div class="clip">
                <h3>Weekend Availability</h3>
                <p>We know that kids don’t just get sick between Monday through Friday so we are open on   Saturdays for appointments and on Sundays for emergencies. </p>
            </div>
        </div>
    </div>
</section>



<section class="samson_extras">
    <div class="site_container">
        <div class="words_candy_standard">
            <div class="words">
                <h2><span class="border_red">Your Health Starts Here</span></h2>
                <p><?php the_field('section_2_text'); ?></p>
            </div>
            <div class="candy" >
                <img src="<?php the_field('section_2_graphic'); ?>">
                
                
            </div>
        </div>

    </div>
</section>

<?php
get_sidebar('review'); ?>

<section class="contact_home">
        <div class="site_container site_container">
            <div class="contact_info">
                <div class="inner_header">
                    <h2><span class="border_red">Contact Us</span></h2>
                </div>

                <div class="contact_group">
                <div class="contact_social">
                    <div class="top_div">
                    <?php the_field('contact'); ?>
                        </div>
                    <div class="bottom_div">
                        <p>Connect With Us</p>
                        <ul>
                            <li><a href="<?php the_field('facebook'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="contact_map">
                      <?php the_field('map'); ?>  
                </div>
                </div>
            </div>

        </div>
</section>

<script>

    
$(function() {
    var header = $("#sticker");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 200) {
            header.addClass("darkHeader");
        } else {
            header.removeClass("darkHeader");
        }
    });
    
//    $('input').bind('focus blur', function() {
//    $('.contact_home').addClass('blend');
//    });
//     $('textarea').bind('focus blur', function() {
//        $('.contact_home').addClass('blend');
//    });
    
});
</script>
<?php
get_footer();
