<?php
/*
 * Template Name: Stages Template
 */

get_header(); ?>

<!--HOME - SPLASH-->


<section class="samson_stages samson_stages_inner samson_stages_detail">
    <div class="site_container">
        <div class="words_candy_standard">
            <div class="words">
                <?php the_field('name'); ?>
                <img src="<?php the_field('number'); ?>">
                <?php the_field('address'); ?>
            </div>
            <div class="candy" >
                <div class="slide">
                    <?php the_field('gallery'); ?>
                </div>
                
            </div>
        </div>
    </div>
    
    <div class="samson-specs"> 
        <div class="site_container">
            <div class="inner_header">
                <h2><span class="border_red_min">Spe</span>cifications</h2>
            </div>
        </div>
        
        <div class="specs-chart">
            <div class="site_container">
                <ul>
                    <li>
                        <img src="<?php the_field('spec_icon_1'); ?>">
                        <p><?php the_field('spec_1'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_2'); ?>">
                        <p><?php the_field('spec_2'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_3'); ?>">
                        <p><?php the_field('spec_3'); ?></p>
                    </li>
                                        <li>
                        <img src="<?php the_field('spec_icon_4'); ?>">
                        <p><?php the_field('spec_4'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_5'); ?>">
                        <p><?php the_field('spec_5'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_6'); ?>">
                        <p><?php the_field('spec_6'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_7'); ?>">
                        <p><?php the_field('spec_7'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_8'); ?>">
                        <p><?php the_field('spec_8'); ?></p>
                    </li>
                    
                    <?php if (get_field('spec_9') != ''): ?>
                    <li>
                        <img src="<?php the_field('spec_icon_9'); ?>">
                        <p><?php the_field('spec_9'); ?></p>
                    </li>
                        <?php endif; ?>   
                    
                    <?php if (get_field('spec_10') != ''): ?>
                    <li>
                        <img src="<?php the_field('spec_icon_10'); ?>">
                        <p><?php the_field('spec_10'); ?></p>
                    </li>
                    <?php endif; ?> 
                    
                    <?php if (get_field('spec_11') != ''): ?>
                    <li>
                        <img src="<?php the_field('spec_icon_11'); ?>">
                        <p><?php the_field('spec_11'); ?></p>
                    </li>
                    <?php endif; ?> 
                </ul>
            </div>
        </div>
        
        <div class="site_container">
            <div class="inner_header">
                <h2><span class="border_red_min">Loc</span>ation</h2>
            </div>
        </div>
        <div class="specs-chart">
            <div class="site_container">
                <ul>
                    <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon-09.png">
                        <p><?php the_field('location'); ?></p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon-010.png">
                        <p><?php the_field('public_transit'); ?></p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon-011.png">
                        <p><?php the_field('by_car'); ?></p>
                    </li>
                </ul>
            </div>
        </div>
        
        <div class="overhead">
            <div class="site_container">
                <div class="inner_header">
                    <h2><span class="border_red_min">Ove</span>rhead View</h2>
                </div>
                
                <div class="floor_plan">
                    <img src="<?php the_field('pdf_image'); ?>">
                    <button class="btn_standard"><a href="<?php the_field('pdf_link'); ?>" target="_blank">Download PDF</a></button>
                </div>
            </div>
        </div>
    </div>
</section>

<!--
<section class="slides slides-alt">

    <div class="center">
      <div class="slide_view" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/samson-stage-1.jpg)">
        </div>
              <div class="slide_view" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/laye-520.jpg)">
        </div>
      <div class="slide_view" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/samson-stage-1.jpg)">
        </div>
              <div class="slide_view" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/laye-520.jpg)">
        </div>
        
    </div>
</section>
-->
<?php
get_footer();
