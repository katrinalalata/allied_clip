<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package samson
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<section class="testimonial">
    <div class="site_container">
        <div class="quote">
            <p>What Our Patients Say</p>
<h4>“Love the entire staff! They are always so professional and courteous on the phone and in person. All four of the doctors that work there are wonderful. They really take their time and address any concerns you may have. Between well care checkups and emergency sick visits I never have an issue making an appointment for my son. They also have separate waiting areas for children who are there for well visits and children who are there for sick visits.“ - <strong>Joanna Murphy</strong> Parent</h4>
        </div>
        
    </div>
</section>