<?php
/*
 * Template Name: Simple Template
 */

get_header(); ?>

<!--HOME - SPLASH-->

<section class="inner">
    <div class="site_container">
        <div class="inner_header">
            <h1><span class="border_red"><?php the_title()?></span></h1>
        </div>
        <div class="inner_content">
                <?php the_field('content'); ?>
                
            </div>
        </div>
</section>




<?php
get_sidebar('review'); ?>


<script>

    
$(function() {
    var header = $("#sticker");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 200) {
            header.addClass("darkHeader");
        } else {
            header.removeClass("darkHeader");
        }
    });
    
//    $('input').bind('focus blur', function() {
//    $('.contact_home').addClass('blend');
//    });
//     $('textarea').bind('focus blur', function() {
//        $('.contact_home').addClass('blend');
//    });
    
});
</script>
<?php
get_footer();