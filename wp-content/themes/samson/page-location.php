<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<!--HOME - SPLASH-->

<section class="home_splash inner_splash" style="background-image:url(<?php the_field('hero_image'); ?>);">
    <div class="site_container">
        <div class="splash_content">
            <h1><?php the_title()?></h1>
        </div>
    </div>
        
</section>

<section class="slides">
    <div class="site_container">
            <div class="inner_content">
                <?php the_field('content'); ?>
                
            </div>
        </div>
</section>



                <div class="map">
                    <?php the_field('map'); ?>
                </div>


<script>

    
$(function() {
    var header = $("#sticker");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 200) {
            header.addClass("darkHeader");
        } else {
            header.removeClass("darkHeader");
        }
    });
    
//    $('input').bind('focus blur', function() {
//    $('.contact_home').addClass('blend');
//    });
//     $('textarea').bind('focus blur', function() {
//        $('.contact_home').addClass('blend');
//    });
    
});
</script>
<?php
get_footer();
