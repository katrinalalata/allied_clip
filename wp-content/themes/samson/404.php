<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package samson
 */

get_header();
?>

<section class="inner">
    <div class="site_container">
        <div class="inner_header">
            <h1><span class="border_red">Oops. Page Not Found</span></h1>
            
                <div class="inner_content">
                    <a href="/">
                <button class="btn_standard">Return Home</button></a>
                
            </div>
            
        </div>
        </div>
</section>


<?php
get_footer();
