<?php


?>

<footer>
    <div class="site_container">
        <div class="samson-footer">
            <div class="footer-links">
                <h2><span class="border_red">Our</span> Practice</h2>
            <?php
			wp_nav_menu( array(
				'theme_location' => 'menu-2',
				'menu_id'        => 'primary-menu',
			) );
			?>
            </div>
            <div class="samson-info">
                <a href="/">
            <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/site-logo.png"></a>
            <?php dynamic_sidebar( 'sidebar-4' ); ?>
            </div>
        </div>
        <div class="samson-social">
<?php dynamic_sidebar( 'sidebar-1' ); ?>
            <?php dynamic_sidebar( 'sidebar-2' ); ?>
            <?php dynamic_sidebar( 'sidebar-3' ); ?>
            
            
        </div>
        <div class="footer_pages">
            <a href="">Allied Physicians Group | Copyright &copy; 2020</a>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
