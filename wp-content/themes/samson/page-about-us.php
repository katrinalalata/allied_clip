<?php

get_header(); ?>

<!--HOME - SPLASH-->

<section class="home_splash inner_splash" style="background-image:url(<?php the_field('hero_image'); ?>);">
    <div class="site_container">
        <div class="splash_content">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
        
</section>

<section class="slides bg_grey">
    <div class="site_container">
            <h2><span class="border_red"><?php the_field('content_title'); ?></span></h2>
            <div class="inner_content">
                <?php the_field('content'); ?>
                
            </div>
        </div>
</section>


<?php
get_sidebar('review'); ?>


<script>

    
$(function() {
    var header = $("#sticker");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 200) {
            header.addClass("darkHeader");
        } else {
            header.removeClass("darkHeader");
        }
    });
    
//    $('input').bind('focus blur', function() {
//    $('.contact_home').addClass('blend');
//    });
//     $('textarea').bind('focus blur', function() {
//        $('.contact_home').addClass('blend');
//    });
    
});
</script>
<?php
get_footer();
