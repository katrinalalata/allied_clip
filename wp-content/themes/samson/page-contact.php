<?php
get_header(); ?>

<!--HOME - SPLASH-->


<section class="contact_home">
        <div class="site_container site_container">
            <div class="contact_info">
                <div class="inner_header">
                    <h2><span class="border_red">Contact Us</span></h2>
                </div>

                <div class="contact_group">
                <div class="contact_social">
                    <div class="top_div">
                    <?php the_field('contact'); ?>
                        </div>
                    <div class="bottom_div">
                        <p>Connect With Us</p>
                        <ul>
                            <li><a href="<?php the_field('facebook'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="contact_map">
                      <?php the_field('map'); ?>  
                </div>
                </div>
            </div>

        </div>
</section>
<?php
get_footer();
